___

This Project is UNMAINTAINED.
___


This CLI tool which helps with provisioning F-Droid.

Simple way to install the latest release of provi on your system:

    sudo pip3 install git+https://gitlab.com/uniqx/provi.git@tags/0.2


useage example:

    # create project folder
    mkdir fdroid_provisions && cd fdroid_provisions

    # fetch example repo file
    mkdir credentials
    wget -P credentials https://gitlab.com/uniqx/provi/raw/master/examples/demo_credentials.yaml

    # generate provision files
    provi assemble

    # deploy one of the generated provisions via adb
    provi provision demo_credentials_user1


## sub commands

### assemble

This subcommand looks for provisioning credentlials in './credentials' and
assembles provisioning files out of them. The provisioning files will be
stored to: './provisions'.

You can find an example for provisioning credentials in
_examples/demo\_credentials.yaml_ (.json is also supported for
credentials, using the same internal structure as .yaml files
in json syntax.)

### genauthjson

Generates a list of usernames and (salted) password-hashes for all users
in all available credential files. Here is and example for such a json file:

    [["user1",
      "e9ff5541c70917cc920c78505e3cfe8cfa8163a8959642b617c264aecb6064dc"],
     ["user2",
      "74f1a096910170f2efc041942d82c10b2e6c0deaaf17abe93b107aac019154d9"]]

This is useful when you are deploying to a http server with a custom
HTTP-Basic-Auth implementation and want to use SHA256 for password hashes.
(eg. node JS)

### provision

This supcommand deploys provision files to devices using adb.

This example deployes an provision to a debug build for fdroid client:

    ./provi provision \
        --app-package org.fdroid.fdroid.debug \
        provisions/demo_credentials_user1.fdrp

If connecting via adb is not an option you can also manually copy
provisioning files to your devices internal storage. (Keep in mind
that the App package name has to be adepted for debug-/rebranded-
F-Droid builds.):

    /storage/emulated/0/Android/data/org.fdroid.fdroid/files/provisions/
